-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 22, 2020 at 03:34 PM
-- Server version: 10.4.8-MariaDB
-- PHP Version: 7.3.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `bb_eye`
--

-- --------------------------------------------------------

--
-- Table structure for table `assignments`
--

CREATE TABLE `assignments` (
  `id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `package_id` int(10) UNSIGNED NOT NULL,
  `module_id` int(10) UNSIGNED NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT 3 COMMENT '* status field can hold\n             * 1 for active module\n             * 2 for in progress module\n             * 3 for in pending module\n             * 4 for completion of module\n             ',
  `coache_id` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `assignments`
--

INSERT INTO `assignments` (`id`, `role_id`, `user_id`, `package_id`, `module_id`, `status`, `coache_id`, `created_at`, `updated_at`) VALUES
(2789, 3, 342, 98, 154, 4, NULL, '2020-01-22 00:24:02', '2020-01-22 00:24:02'),
(2790, 3, 342, 100, 157, 4, NULL, '2020-01-22 00:28:29', '2020-01-22 00:28:29'),
(2791, 3, 342, 97, 155, 4, NULL, '2020-01-22 00:29:49', '2020-01-22 00:29:49'),
(2792, 3, 342, 103, 155, 4, NULL, '2020-01-22 00:30:52', '2020-01-22 00:30:52'),
(2793, 2, 372, 98, 154, 3, 2789, NULL, NULL),
(2794, 2, 372, 98, 152, 3, 2789, NULL, NULL),
(2795, 3, 342, 102, 157, 4, NULL, '2020-01-22 00:55:32', '2020-01-22 00:55:32'),
(2796, 3, 342, 101, 154, 4, NULL, '2020-01-22 01:12:56', '2020-01-22 01:12:56');

-- --------------------------------------------------------

--
-- Table structure for table `discussions`
--

CREATE TABLE `discussions` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `question_id` int(10) UNSIGNED NOT NULL,
  `response_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `assignment_id` int(10) UNSIGNED NOT NULL,
  `visibility` tinyint(4) NOT NULL DEFAULT 0 COMMENT '* status field can hold * 1 for client only * 0 for all	'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `documents`
--

CREATE TABLE `documents` (
  `id` int(10) UNSIGNED NOT NULL,
  `filename` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `uploaded_by` int(11) NOT NULL,
  `uploaded_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `module_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `documents`
--

INSERT INTO `documents` (`id`, `filename`, `description`, `uploaded_by`, `uploaded_at`, `created_at`, `updated_at`, `module_id`) VALUES
(453, 'BB_155.docx', NULL, 342, '2020-01-22 11:52:02', '2020-01-21 22:52:02', '2020-01-21 22:52:02', 155);

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `modules`
--

CREATE TABLE `modules` (
  `id` int(10) UNSIGNED NOT NULL,
  `module_category_id` int(11) DEFAULT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `is_live` tinyint(1) NOT NULL DEFAULT 0,
  `status` tinyint(1) NOT NULL DEFAULT 1,
  `author_id` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `modules`
--

INSERT INTO `modules` (`id`, `module_category_id`, `title`, `description`, `content`, `created_at`, `updated_at`, `is_live`, `status`, `author_id`) VALUES
(152, 3, 'Testing', 'Testing', '<p><font color=\"#c41a16\" face=\"Consolas, Lucida Console, Courier New, monospace\"><span style=\"font-size: 12px; white-space: pre;\">Testing</span></font><br></p>', '2020-01-21 01:07:03', '2020-01-21 01:48:38', 1, 1, 342),
(153, 3, 'tester', 'tester', '<p>tester<br></p>', '2020-01-21 01:10:07', '2020-01-21 01:19:52', 1, 1, 342),
(154, 3, 'Testing', 'Testing', '<p>Testing<br></p>', '2020-01-21 01:18:47', '2020-01-21 01:20:08', 1, 1, 342),
(155, 2, 'Turtle Beach', 'Testing', '<p>Tesr</p>', '2020-01-21 01:24:03', '2020-01-21 01:28:06', 1, 1, 342),
(156, 2, 'Testing', 'Testing', '<p>testing</p>', '2020-01-21 01:29:51', '2020-01-21 01:30:24', 1, 1, 342),
(157, 2, 'Turtle Beach test', 'Testing', '<p>efdsff</p>', '2020-01-21 01:34:05', '2020-01-21 01:34:21', 1, 1, 342);

-- --------------------------------------------------------

--
-- Table structure for table `module_category`
--

CREATE TABLE `module_category` (
  `id` int(11) NOT NULL,
  `cat_name` varchar(500) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `module_category`
--

INSERT INTO `module_category` (`id`, `cat_name`) VALUES
(2, 'ABC'),
(3, 'XYZ');

-- --------------------------------------------------------

--
-- Table structure for table `packages`
--

CREATE TABLE `packages` (
  `id` int(10) UNSIGNED NOT NULL,
  `package_category_id` int(11) DEFAULT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `price` double(8,2) DEFAULT NULL,
  `currency` char(4) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `paymnent_frequency` enum('One Off','monthly','weekly','yearly') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `facebook_group` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `release_schedule` enum('deliver immediately','rolling launch','one off launch','on completion of previous') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT 0 COMMENT '* status field can hold* 1 for reply off * 0 for reply on'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `packages`
--

INSERT INTO `packages` (`id`, `package_category_id`, `title`, `description`, `price`, `currency`, `paymnent_frequency`, `facebook_group`, `release_schedule`, `created_at`, `updated_at`, `status`) VALUES
(96, 31, 'Testing', '<p>Testing<br></p>', 23.00, 'AUD', 'One Off', 'Testing', NULL, '2020-01-15 19:16:44', '2020-01-21 00:09:46', 0),
(97, 28, 'Test Again', '<p>Test Again<br></p>', 234.00, 'AUD', 'One Off', 'Testing', NULL, '2020-01-20 23:15:41', '2020-01-20 23:18:54', 0),
(98, 26, 'Testing', '<p>tesdf</p>', 32.00, 'CAD', 'One Off', 'Testing', NULL, '2020-01-20 23:17:17', '2020-01-21 00:09:55', 0),
(99, 30, 'Tester', '<p>Test<br></p>', 1213.00, 'AUD', 'One Off', 'Testing', NULL, '2020-01-20 23:37:05', '2020-01-21 00:10:06', 0),
(100, 26, 'Tester', '<p>Test<br></p>', 1213.00, 'AUD', 'One Off', 'Testing', NULL, '2020-01-20 23:38:34', '2020-01-21 00:10:49', 0),
(101, 29, 'ABCDEFG', '<p>DEF</p>', 2000.00, 'USD', 'One Off', 'DEF', NULL, '2020-01-20 23:43:05', '2020-01-20 23:43:05', 0),
(102, 31, 'LIMONS', '<p>LIMONS<br></p>', 2323.00, 'EUR', 'One Off', 'Testing', NULL, '2020-01-21 00:12:28', '2020-01-21 00:12:49', 0),
(103, 28, 'Ahsan Amin', '<p>Ahsan Amin</p>', 2020.00, 'USD', 'One Off', 'Testing', NULL, '2020-01-21 22:11:01', '2020-01-21 22:11:01', 0);

-- --------------------------------------------------------

--
-- Table structure for table `package_category`
--

CREATE TABLE `package_category` (
  `id` int(11) NOT NULL,
  `cat_name` varchar(500) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `package_category`
--

INSERT INTO `package_category` (`id`, `cat_name`) VALUES
(26, 'Test'),
(27, 'Testing'),
(28, 'ABC'),
(29, 'XYZ'),
(30, 'DEF'),
(31, 'GHI');

-- --------------------------------------------------------

--
-- Table structure for table `package_module`
--

CREATE TABLE `package_module` (
  `module_id` int(10) UNSIGNED NOT NULL,
  `package_id` int(10) UNSIGNED NOT NULL,
  `id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `package_module`
--

INSERT INTO `package_module` (`module_id`, `package_id`, `id`, `created_at`, `updated_at`) VALUES
(154, 98, 806, '2020-01-22 00:23:12', '2020-01-22 00:23:12'),
(152, 98, 807, '2020-01-22 00:23:12', '2020-01-22 00:23:12'),
(157, 100, 808, '2020-01-22 00:28:22', '2020-01-22 00:28:22'),
(152, 100, 809, '2020-01-22 00:28:22', '2020-01-22 00:28:22'),
(155, 97, 810, '2020-01-22 00:29:41', '2020-01-22 00:29:41'),
(152, 97, 811, '2020-01-22 00:29:41', '2020-01-22 00:29:41'),
(155, 103, 812, '2020-01-22 00:30:43', '2020-01-22 00:30:43'),
(157, 103, 813, '2020-01-22 00:30:43', '2020-01-22 00:30:43'),
(152, 103, 814, '2020-01-22 00:30:43', '2020-01-22 00:30:43'),
(157, 102, 815, '2020-01-22 00:55:24', '2020-01-22 00:55:24'),
(152, 102, 816, '2020-01-22 00:55:24', '2020-01-22 00:55:24'),
(154, 101, 817, '2020-01-22 01:12:47', '2020-01-22 01:12:47'),
(152, 101, 818, '2020-01-22 01:12:47', '2020-01-22 01:12:47');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `password_resets`
--

INSERT INTO `password_resets` (`email`, `token`, `created_at`) VALUES
('sadaf_cu@hotmail.com', '$2y$10$/DVMaSxxAXfII5oLcKyKy.ilydGF7PbAzT30LLG/Y6pZ23Iz6SoEu', '2017-05-22 16:51:34'),
('juliemcgloin@hotmail.com', '$2y$10$OSq9D.8zwqrbjLcY7fBnyuY/8yhwzPp8fyh2UaVrwABclLT4CBaVy', '2017-06-20 23:44:09'),
('kazim.raza@mksofttech.com', '$2y$10$bgFL/gGsIzoOOHiElpHTyedSXVBAESkQoOFTzE9gEG8Vwhdquie32', '2017-06-21 20:30:39'),
('zamin2212@gmail.com', '$2y$10$6RF/4akBqJ/E7pAqmV1kUeYCcAVeUI/hMB0DOsy199YB5nWyZUlcC', '2018-02-21 18:30:55'),
('info@regeneratehealthmc.com', '$2y$10$wDnqKmoSc8FJVuEXX2AbL..eO7T/r8Brmbgo3VI67t59OIavtGtJC', '2018-03-03 15:00:05'),
('jnelle.holland@gmail.com', '$2y$10$vtkv1dcvslufW4EdOLaVUOwYDDVau0pCd7cNT1Qnq8JBmRa7dyZnm', '2018-09-15 12:59:37'),
('paul.cashman.b87q@statefarm.com', '$2y$10$UXtdKVZOgrPP0aG86P1WNujl1GxtvCr4M1qom7xP30shDztP8wy.C', '2019-02-05 13:14:35'),
('muhammadarsalan11092@gmail.com', '$2y$10$rSacATcB2HtnwLm7CUFQtemVrtqHs4n6EAQP/AASTbFJRZFAj99SO', '2019-02-07 07:22:10'),
('kr_sayani@outlook.com', '$2y$10$ucIB4L9TTzzojKuE.sImf.Gjw.xf0N4DrK9L7NuPXxRj.MPHi17xe', '2019-02-07 07:43:23'),
('kr_sayani@hotmail.com', '$2y$10$DdKCQhoBFaqIvtTYU6G3D.HR0meTFHTWbHhoeZFpireYPhOCH.g1q', '2019-02-07 07:50:11'),
('faizanshaikh@hztech.biz', '$2y$10$YwwhItzHGMdTC4EjvMIV3.4U2/iE4WaIfSMi0/LRl8nA4zkhpNHWS', '2019-02-07 09:37:13'),
('nilton.rodrigues.fgbo@statefarm.com', '$2y$10$rzSiReCUbERtPRwFlYs1j.BDfIx0g6zQ/YUaSBqEM.bqYxPNYWrH.', '2019-05-16 16:46:57'),
('shahin.chear.rnpk@statefarm.com', '$2y$10$/AqeOjrPIAVwoHpbVSeDHO3vKxOByn0jrh35fV8SrDJlTu7WX2bp6', '2019-05-20 04:26:42'),
('paul.lavelle.j8ov@statefarm.com', '$2y$10$V3tZG3.wFS14wGoP2ufbyeqnJOj7RCGxhPOuab7/TXyZIx3QQvYMa', '2019-06-05 13:28:48'),
('linda.horton.nsd4@statefarm.com', '$2y$10$pDyKF6cPwmSBfZ//K38ZpemR0w6Zlqx5xr0xT0NU9nDdwamYOFS32', '2019-07-01 01:43:15'),
('wendy.m.baker@fbfs.com', '$2y$10$XeoNfTHdPH7Zvglv09hQmu16tpCZ1D/eBiCdnfe0wpmaKp2IBunK.', '2019-07-11 14:54:42'),
('kelly.lux.gjcg@statefarm.com', '$2y$10$qXbu6BnNrSPuQhP3xqb36OAqGYSBdzPT6FQ/vIqSCRNrDLR8IllSm', '2020-01-08 17:32:17');

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE `permissions` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `permission_role`
--

CREATE TABLE `permission_role` (
  `id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL,
  `permission_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `questions`
--

CREATE TABLE `questions` (
  `id` int(10) UNSIGNED NOT NULL,
  `sno` int(11) NOT NULL,
  `content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `module_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `questions`
--

INSERT INTO `questions` (`id`, `sno`, `content`, `created_at`, `updated_at`, `module_id`) VALUES
(1014, 1, '<p>Who are you?</p>', '2020-01-21 22:51:56', '2020-01-21 22:51:56', 155);

-- --------------------------------------------------------

--
-- Table structure for table `responses`
--

CREATE TABLE `responses` (
  `id` int(10) UNSIGNED NOT NULL,
  `content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `responses`
--

INSERT INTO `responses` (`id`, `content`, `created_at`, `updated_at`) VALUES
(4695, 'hello world', '2020-01-15 19:10:31', '2020-01-15 19:10:31'),
(4696, 'hie', '2020-01-15 19:26:18', '2020-01-15 19:26:18'),
(4697, 'hrtrr', '2020-01-15 19:26:42', '2020-01-15 19:26:42'),
(4698, 'hie', '2020-01-15 19:27:41', '2020-01-15 19:27:41'),
(4699, 'hello', '2020-01-15 22:04:52', '2020-01-15 22:04:52'),
(4700, 'hello', '2020-01-15 22:05:29', '2020-01-15 22:05:29'),
(4701, 'hello', '2020-01-15 22:06:23', '2020-01-15 22:06:23'),
(4702, 'hello', '2020-01-15 22:09:18', '2020-01-15 22:09:18'),
(4703, 'jjknjk', '2020-01-15 22:09:29', '2020-01-15 22:09:29'),
(4704, 'sds', '2020-01-15 23:04:50', '2020-01-15 23:04:50'),
(4705, 'ok sir', '2020-01-15 23:42:58', '2020-01-15 23:42:58'),
(4706, 'ahsan here', '2020-01-15 23:44:19', '2020-01-15 23:44:19'),
(4707, 'ahsan here', '2020-01-15 23:44:43', '2020-01-15 23:44:43'),
(4708, 'hello', '2020-01-15 23:58:59', '2020-01-15 23:58:59'),
(4709, 'ok sure', '2020-01-15 23:59:23', '2020-01-15 23:59:23'),
(4710, '<p><b>hello,&nbsp;</b></p><p><b>How are you</b></p>', '2020-01-19 19:39:17', '2020-01-19 19:39:17');

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `desc` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `desc`, `created_at`, `updated_at`) VALUES
(1, 'admin', '', NULL, NULL),
(2, 'client', '', NULL, NULL),
(3, 'coache', '', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `avatar` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'default.jpg',
  `description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`, `avatar`, `description`, `status`) VALUES
(342, 'ahsan amin', 'ahsan.amin334@gmail.com', '$2y$10$Vb07Pi09RUZDcdjSBMD2iuV31l2QoRmP20GHYqMohd3Gt7Ssxx9iG', NULL, '2020-01-15 18:45:29', '2020-01-15 18:45:29', 'default.jpg', NULL, 1),
(347, 'yasin', 'yasin@gmail.com', '$2y$10$kJztlF.SKVUkPeUz7igemewvbbR3FBbyVwrOGMkjmPEvEGaNV7/wC', NULL, '2020-01-15 19:22:55', '2020-01-17 02:32:40', 'default.jpg', NULL, 0),
(348, 'Tahseen', 'tahseen@gmail.com', '$2y$10$U4t48SA2Y9EpiCDFO2sh8.AKJHomE/qZl9TS7KFrfHhAIckmR8Ppa', NULL, '2020-01-15 22:03:18', '2020-01-15 22:03:18', 'default.jpg', NULL, 0),
(349, 'khurram', 'khurram@gmail.com', '$2y$10$v.p9AH0y/ucHf/9VAyFRXuAdVivlzwCK.yFsuN5fb3BgaHwOOKhS.', NULL, '2020-01-15 23:57:38', '2020-01-15 23:57:38', 'default.jpg', NULL, 0),
(362, 'asad', 'asad@gmail.com', '$2y$10$dAqkXOEvxd2K2FEYlwLUM.UwuwS273fp3z.5h28cd0LEIJGLzqdpK', NULL, '2020-01-16 21:39:44', '2020-01-16 23:32:17', 'default.jpg', NULL, 0),
(368, 'yaseen', 'yaseen@gmail.com', '$2y$10$Om65gDpGFB.8lwEWcZBA7e9IAZBCX3U1KNG8JhFykdGqv6MTRWjIW', NULL, '2020-01-17 01:51:32', '2020-01-17 01:51:32', 'default.jpg', NULL, 0),
(370, 'abc', 'abc@gmail.com', '$2y$10$0GG5UNl0aYHtsIuU0e6QgOrZ.eHtDUB9PICMUPB1gK21Pl7BKupm.', NULL, '2020-01-21 23:02:36', '2020-01-21 23:02:36', 'default.jpg', NULL, 0),
(371, 'def', 'def@gmail.com', '$2y$10$WAK0poveUxuygJH7oEe6Uu1MFQlM2nkZhhLoplDwwRz3Pm2CHHjOG', NULL, '2020-01-21 23:56:44', '2020-01-21 23:56:44', 'default.jpg', NULL, 0),
(372, 'xyz', 'xyz@gmail.com', '$2y$10$HPrTr1Y4BIUMN9kZ5J7IJe6FkGtZg/z0t61SqqCLJGBbrTBnYzFIW', NULL, '2020-01-22 00:32:38', '2020-01-22 00:32:38', 'default.jpg', NULL, 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `assignments`
--
ALTER TABLE `assignments`
  ADD PRIMARY KEY (`id`),
  ADD KEY `role_user_package_role_id_index` (`role_id`),
  ADD KEY `role_user_package_user_id_index` (`user_id`),
  ADD KEY `role_user_package_package_id_index` (`package_id`),
  ADD KEY `assignments_module_id_index` (`module_id`),
  ADD KEY `assignments_coache_id_index` (`coache_id`);

--
-- Indexes for table `discussions`
--
ALTER TABLE `discussions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_response_question_user_id_index` (`user_id`),
  ADD KEY `user_response_question_question_id_index` (`question_id`),
  ADD KEY `user_response_question_response_id_index` (`response_id`),
  ADD KEY `discussions_assignment_id_index` (`assignment_id`);

--
-- Indexes for table `documents`
--
ALTER TABLE `documents`
  ADD PRIMARY KEY (`id`),
  ADD KEY `documents_module_id_index` (`module_id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `modules`
--
ALTER TABLE `modules`
  ADD PRIMARY KEY (`id`),
  ADD KEY `modules_author_id_foreign` (`author_id`);

--
-- Indexes for table `module_category`
--
ALTER TABLE `module_category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `packages`
--
ALTER TABLE `packages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `package_category`
--
ALTER TABLE `package_category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `package_module`
--
ALTER TABLE `package_module`
  ADD KEY `package_module_module_id_index` (`module_id`),
  ADD KEY `package_module_package_id_index` (`package_id`),
  ADD KEY `id` (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`),
  ADD KEY `password_resets_token_index` (`token`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `permission_role`
--
ALTER TABLE `permission_role`
  ADD PRIMARY KEY (`id`),
  ADD KEY `permission_role_role_id_index` (`role_id`),
  ADD KEY `permission_role_permission_id_index` (`permission_id`);

--
-- Indexes for table `questions`
--
ALTER TABLE `questions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `questions_module_id_index` (`module_id`);

--
-- Indexes for table `responses`
--
ALTER TABLE `responses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `assignments`
--
ALTER TABLE `assignments`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2797;

--
-- AUTO_INCREMENT for table `discussions`
--
ALTER TABLE `discussions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4711;

--
-- AUTO_INCREMENT for table `documents`
--
ALTER TABLE `documents`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=454;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `modules`
--
ALTER TABLE `modules`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=158;

--
-- AUTO_INCREMENT for table `module_category`
--
ALTER TABLE `module_category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `packages`
--
ALTER TABLE `packages`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=104;

--
-- AUTO_INCREMENT for table `package_category`
--
ALTER TABLE `package_category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;

--
-- AUTO_INCREMENT for table `package_module`
--
ALTER TABLE `package_module`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=819;

--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `permission_role`
--
ALTER TABLE `permission_role`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `questions`
--
ALTER TABLE `questions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1015;

--
-- AUTO_INCREMENT for table `responses`
--
ALTER TABLE `responses`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4711;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=373;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `assignments`
--
ALTER TABLE `assignments`
  ADD CONSTRAINT `assignments_coache_id_foreign` FOREIGN KEY (`coache_id`) REFERENCES `assignments` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `assignments_module_id_foreign` FOREIGN KEY (`module_id`) REFERENCES `modules` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `role_user_package_package_id_foreign` FOREIGN KEY (`package_id`) REFERENCES `packages` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `role_user_package_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `role_user_package_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `discussions`
--
ALTER TABLE `discussions`
  ADD CONSTRAINT `discussions_assignment_id_foreign` FOREIGN KEY (`assignment_id`) REFERENCES `assignments` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `user_response_question_question_id_foreign` FOREIGN KEY (`question_id`) REFERENCES `questions` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `user_response_question_response_id_foreign` FOREIGN KEY (`response_id`) REFERENCES `responses` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `user_response_question_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `documents`
--
ALTER TABLE `documents`
  ADD CONSTRAINT `documents_module_id_foreign` FOREIGN KEY (`module_id`) REFERENCES `modules` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `modules`
--
ALTER TABLE `modules`
  ADD CONSTRAINT `modules_author_id_foreign` FOREIGN KEY (`author_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `package_module`
--
ALTER TABLE `package_module`
  ADD CONSTRAINT `package_module_module_id_foreign` FOREIGN KEY (`module_id`) REFERENCES `modules` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `package_module_package_id_foreign` FOREIGN KEY (`package_id`) REFERENCES `packages` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `permission_role`
--
ALTER TABLE `permission_role`
  ADD CONSTRAINT `permission_role_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `permission_role_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `questions`
--
ALTER TABLE `questions`
  ADD CONSTRAINT `questions_module_id_foreign` FOREIGN KEY (`module_id`) REFERENCES `modules` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
