<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Http\Request;

Route::get('/', function () {
    return view('welcome');
});

Route::get('/clear-cache', function() {
    $exitCode = Artisan::call('cache:clear');
    // return what you want
});

Auth::routes();

Route::get('/home', 'HomeController@index');

   //  Package Category Routes
Route::get('packages/view_category', 'PackageController@view_category')->middleware('auth')->name('view_package_category');
Route::get('packages/add_category', 'PackageController@add_category')->middleware('auth')->name('add_package_category');
Route::post('packages/store_category/', 'PackageController@store_category')->middleware('auth')->name('store_package_category');
Route::get('packages/edit_category/{id}', 'PackageController@edit_category')->middleware('auth')->name('edit_package_category');
Route::post('packages/update_category/{id}', 'PackageController@update_category')->middleware('auth')->name('update_package_category');
Route::delete('packages/delete_category/{id}', 'PackageController@destroy_category')->middleware('auth')->name('destroy_package_category');
Route::get('packages/package_category', 'PackageController@package_category')->middleware('auth')->name('package_category');
Route::get('packages/get_packages_by_category/{id}', 'PackageController@get_packages_by_category')->middleware('auth')->name('get_packages_by_category');
//  Package Category Routes

   //  Module Category Routes
   Route::get('modules/view_category', 'ModuleController@view_category')->middleware('auth')->name('view_module_category');
   Route::get('modules/add_category', 'ModuleController@add_category')->middleware('auth')->name('add_module_category');
   Route::post('modules/store_category/', 'ModuleController@store_category')->middleware('auth')->name('store_module_category');
   Route::get('modules/edit_category/{id}', 'ModuleController@edit_category')->middleware('auth')->name('edit_module_category');
   Route::post('modules/update_category/{id}', 'ModuleController@update_category')->middleware('auth')->name('update_module_category');
   Route::delete('modules/delete_category/{id}', 'ModuleController@destroy_category')->middleware('auth')->name('destroy_module_category');
   Route::get('modules/module_category', 'ModuleController@module_category')->middleware('auth')->name('module_category');
   Route::get('modules/get_modules_by_category/{id}', 'ModuleController@get_modules_by_category')->middleware('auth')->name('get_modules_by_category');
   
   //  Module Category Routes
   

      Route::get('/modules', 'ModuleController@index')->middleware('auth');

      Route::get('/modules/add/{id}', 'ModuleController@create')->middleware('auth');
      Route::get('/modules/add', 'ModuleController@create_without_id')->middleware('auth');
      
      Route::get('modules/{module_id?}','ModuleController@edit')->middleware('auth');
      Route::post('modules','ModuleController@store')->middleware('auth');
      Route::put('modules/{module_id?}','ModuleController@update')->middleware('auth');
      Route::delete('modules/{module_id?}','ModuleController@destroy')->middleware('auth');
      Route::put('modules/make_live/{module_id?}','ModuleController@makeLive')->middleware('auth');
      Route::get('modules/live','ModuleController@getLive')->middleware('auth');
      Route::get('modules/preview/{module_id?}','ModuleController@show')->middleware('auth');
      Route::post('modules/make_copy/{module_id}','ModuleController@makeCopy')->middleware('auth');

Route::post('/documents/upload', 'DocumentController@docUploadPost')->middleware('auth');
//Route::get('/documents/list/{module_id?}', 'DocumentController@listModuleDoc')->middleware('auth');
Route::get('/documents/list/{module_id?}', 'DocumentController@listModuleDocCoach')->middleware('auth');
Route::delete('/documents/{doc_id?}', 'DocumentController@destroy')->middleware('auth');


Route::group(['prefix' => 'questions','middleware' => 'auth'], function () {
     Route::put('sort','QuestionController@sort');
    Route::get('list/{module_id?}', 'QuestionController@grid');
    Route::post('/','QuestionController@store');
    Route::put('{question_id}','QuestionController@update');
    Route::get('{question_id}','QuestionController@show');
    Route::delete('{question_id}','QuestionController@destroy');
   
    
});

Route::group(['prefix' => 'packages','middleware' => 'auth'], function () {
    Route::get('/add/{id}', 'PackageController@create');
    Route::get('/add', 'PackageController@create_without_id');
    Route::get('/', 'PackageController@index');
    Route::post('/','PackageController@store');
    Route::put('{package_id}','PackageController@update');
    Route::get('{package_id}','PackageController@show');
    Route::delete('{package_id}','PackageController@destroy');
    Route::get('/linked_clients/{package_id}','PackageController@showLinkedClients');
    Route::post('/make_copy/{package_id}','PackageController@makeCopy');
    Route::post('/status/{package_id}','PackageController@updateStatus');
    Route::get('/assign_coach/{package_id}','PackageController@assignCoachForm');
    Route::post('/assign_coach','PackageController@assignCoach');
    Route::get('/view/{package_id}','PackageController@view');
    //  Package Category Routes
    
});

Route::group(['prefix' => 'clients','middleware' => 'auth'], function () {
    Route::get('/active_packages','ClientController@activePackages');
    Route::get('/', 'ClientController@index');
    Route::post('/','ClientController@store');
    Route::post('/addExisting','ClientController@storeExisting');
    Route::put('{package_id}','ClientController@update');
//    Route::get('{package_id}','ClientController@show');
    Route::delete('{client_id}','ClientController@destroy');
    Route::post('/reset','ClientController@client_pass_reset')->name('clientpassreset');
});

Route::group(['prefix' => 'coaches','middleware' => 'auth'], function () {
    Route::get('/', 'CoacheController@index');
    Route::post('/','CoacheController@store');
    Route::get('/active_packages','CoacheController@activePackages');
    Route::delete('/{coach_id}','CoacheController@destroy');
    Route::post('/status','CoacheController@updateStatus');
    Route::post('/upload','CoacheController@uploadEditor');
});

Route::group(['prefix' => 'assigned','middleware' => 'auth'], function () {
    Route::post('/sendtoclient/{assigned_id}', 'AssignmentController@sendtoclient');
    Route::post('/sendtocoach/{assigned_id}', 'AssignmentController@sendtocoach');
    Route::post('/savecontinue/{assigned_id}', 'AssignmentController@savecontinue');
    Route::get('/{assigned_id}', 'AssignmentController@show');
    Route::post('/update/{assigned_id}/', 'AssignmentController@update');
    Route::post('/{package_id}/{module_id}', 'AssignmentController@store');
    Route::post('/update_status', 'AssignmentController@updateStatus');
   
});

Route::group(['prefix' => 'profile','middleware' => 'auth'], function () {
    Route::get('/', 'ProfileController@index');
    Route::post('/','ProfileController@store');
    Route::put('/update/{user_id}','ProfileController@update');
    Route::get('/edit/{user_id}','ProfileController@edit');
    Route::get('/{user_id}','ProfileController@show');
    Route::delete('{user_id}','ProfileController@destroy');
    Route::get('/linked_clients/{user_id}','ProfileController@showLinkedClients');
});


/*Route::get('/checking/mail', function(){
     $data["mail_message"] = "Hello!";
     Mail::send([], [], function ($message) {
         $message
             ->to('kazim03031990@gmail.com')
             ->from('no-reply@business-bullseye.com')
             ->subject('TEST')
             ->setBody('<h1>Hi, welcome user!</h1>', 'text/html'); // for HTML rich messages
       });
    
         //$msg = "First line of text\nSecond line of text";
    
         // use wordwrap() if lines are longer than 70 characters
         //$msg = wordwrap($msg,70);
    
         // send email
         //mail("eagleeye110@gmail.com","My subject",$msg);
    echo phpinfo();
});*/