@extends('layouts.app')

@section('content')
<div class="">
    <div class="row">
        <div class="col-md-9 ">

            @php
                if(isset($module_category)){
                   $cat_name= $module_category->cat_name;
                }
            @endphp

        @if(!isset($module_category))
        <form method="post" class="form-horizontal" enctype="multipart/form-data" action="{{route('store_module_category')}}">
        @else
        <form method="post" class="form-horizontal" enctype="multipart/form-data" action="{{route('update_module_category',['id'=>$module_category->id])}}">
        @endif
                <div class="modal-body">
                    {{ csrf_field() }}
                    <div class="form-group error">
                     <?php   if(isset($module_category)){  ?>
              
                            <label for="inputName" class="col-sm-3 control-label">Title</label>
                            <div class="col-sm-6">
                                <input type="text" class="form-control has-error" id="title" name="title"  placeholder="Category Name" value="{{old('title',$cat_name)}}">
                            </div>
                        <div class="col-sm-3">
                            <button type="submit" class="btn btn-success" id="btn-save-module" value="">Update</button>
                     
                        </div>
                    <?php } else { ?>
                        <label for="inputName" class="col-sm-3 control-label">Title</label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control has-error" id="title" name="title"  placeholder="Category Name" value="">
                        </div>
                    <div class="col-sm-3">
                        <button type="submit" class="btn btn-primary" id="btn-save-module" value="">Add</button>
                    </div>
                    <?php } ?>
                    </div> 
            </form>
        </div>
    </div>
</div>
@endsection
@section('css')
@parent
<style>
    .selected {
        background:yellow !important;
    }
    .hidden {
        display:none !important;
    }
    .ui-sortable-placeholder {
        background:greenyellow !important;
    }
</style>
<link href="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.1/summernote.css" rel="stylesheet">

<!-- include summernote css/js-->
<link href="{{asset('css/summernote.css')}}">
<!-- include codemirror (codemirror.css, codemirror.js, xml.js, formatting.js) -->
<link rel="stylesheet" type="text/css" href="//cdnjs.cloudflare.com/ajax/libs/codemirror/3.20.0/codemirror.css">
<link rel="stylesheet" type="text/css" href="//cdnjs.cloudflare.com/ajax/libs/codemirror/3.20.0/theme/monokai.css">
@endsection

@section('script')
@parent

<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/jquery.bootstrapvalidator/0.5.2/css/bootstrapValidator.min.css"/>
<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/jquery.bootstrapvalidator/0.5.2/js/bootstrapValidator.min.js"></script>

<script src="../js/bootstrap-formhelpers.js"></script>
<script src="../js/bootstrap-formhelpers-currencies.js"></script>
<script src="{{ asset('js/jquery-ui.js') }}"></script>
<script>

$(document).ready(function () {
    
});

</script>

<!--<script src="{{ asset('js/jquery-ui.min.js') }}"></script>
<script>
$('.sortable').sortable({
  update: function(){
     console.log('sortable updated'); 
  }
});
</script>-->
<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/codemirror/3.20.0/codemirror.js"></script>
<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/codemirror/3.20.0/mode/xml/xml.js"></script>
<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/codemirror/2.36.0/formatting.js"></script>
<script src="{{asset('js/summernote.min.js?v=1')}}"></script>
<script>

  
</script>

@endsection

@section('heading')
Module 
@endsection

@section('title')
Modules
@endsection

@section('breadcrumbs')
New Package
@endsection