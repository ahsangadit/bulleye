<?php

namespace App\Http\Controllers;

use App\package;
use App\package_category;
use Illuminate\Http\Request;
use App\module;
use App\Mail\NewPackageAdded;
use Auth;

class PackageController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $packages = package::owner()->active()->get();
        // dd($packages);
        $epackage = new package();
        $live_modules = module::where('is_live', true)->author()->get();
        //$allcoaches = $epackage->getAllCoaches(32);
        //$assigns = \App\assign::where('user_id',Auth::id())->distinct()->get();
        // foreach($assigns as $assign){
        //     $clients = \App\assign::where('coache_id',$assign->id)->distinct()->get();

        //     foreach($clients as $client) {
        //         $users = \App\User::where('id',$client->user_id)->distinct()->get();
        //         foreach(  $users as $user){
        //             echo   $user->email.'<br>';
        //         }
        //     }
        // }
   
        // return view('package.index')->with('packages', $packages)->with('epackage', $epackage)
        //                ->with('live_modules', $live_modules)->with('allcoaches', $allcoaches);

        return view('package.index')->with('packages', $packages)->with('epackage', $epackage)
        ->with('live_modules', $live_modules);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id) {
        //
        $epackage = new package();
        $live_modules = module::where('is_live', true)->author()->get();
        $package_category = package_category::get();
        $cat_id = $id;

        return view('package.create')->with('epackage', $epackage)->with('state', 'add')
                        ->with('live_modules', $live_modules)->with('package_category',$package_category)->with('cat_id',$cat_id);
    }

    public function create_without_id() {
        $epackage = new package();
        $live_modules = module::where('is_live', true)->author()->get();
        $package_category = package_category::get();

        return view('package.create_without')->with('epackage', $epackage)->with('state', 'add')
                        ->with('live_modules', $live_modules)->with('package_category',$package_category);
   
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
 
//        $package = package::create($request->all());
         $package = new package();
   //     dd($package);
 //       $package = package::owner()->find($package_id);
        $package->title = $request->title;
        $package->package_category_id = $request->package_category;
        $package->description = $request->description;
        $package->price = $request->price;
        $package->currency = $request->currency;
        $package->release_schedule = $request->release_schedule;
        $package->paymnent_frequency = $request->paymnent_frequency;
        $package->facebook_group = $request->facebook_group;
//        $package->selected_modulels = $request->selected_modules;
        $package->save();
     
        $package->modules()->attach($request->selected_modules);
     
        // auto set coache
        $assign = new \App\assign();

//        if (auth()->user()->status == 2)
        $assign->coache(auth()->user()->id, $package->id);
//        else
//            $assign->coache($request->coach, $package->id);



        return response()->json($package);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\package  $package
     * @return \Illuminate\Http\Response
     */
    public function show($package_id) {
        $package = package::owner()->find($package_id);
        
        $this->authorize('edit', $package);

//        $package->selected_modules ='["2","5"]';  //$package->modules()->get();
//        return response()->json($package);
        $epackage = new package();
        $live_modules = module::where('is_live', true)->author()->get();
        $package_category = package_category::get();

        return view('package.create')->with('epackage', $epackage)->with('state', 'update')
                        ->with('live_modules', $live_modules)->with("package", $package)->with('package_category',$package_category);;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\package  $package
     * @return \Illuminate\Http\Response
     */
    public function edit($package_id) {
        $package = package::find($package_id);

//        return response()->json($package)->with('selected_modules',$package->modules()->get());
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\package  $package
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $package_id) {
      
        $package = package::owner()->find($package_id);
        $package->title = $request->title;
        $package->package_category_id = $request->package_category;
        $package->description = $request->description;
        $package->price = $request->price;
        $package->currency = $request->currency;
        $package->release_schedule = $request->release_schedule;
        $package->paymnent_frequency = $request->paymnent_frequency;
        $package->facebook_group = $request->facebook_group;
//        $package->selected_modulels = $request->selected_modules;
        $package->save();
       
        $package->setSelectedModulesAttribute($request->selected_modules);
        $assignment = \App\assignment::where('package_id', $package->id)->whereNotIn('module_id', $request->selected_modules)->delete();

        $previous = \App\assignment::where("package_id", $package->id)->pluck('module_id')->unique("moudle_id")->toArray();
     
        $selected = $request->selected_modules;

        $new = array_diff($selected, $previous);

        $role_id = \App\role::client();
        if (count($new) > 0) {

            foreach ($new as $module_id) {
                $clients = \App\assignment::where("package_id", $package->id)->where("role_id", $role_id)->get()->unique("user_id");
                foreach ($clients as $client) {
                    \App\assignment::create(['role_id' => $role_id, 'user_id' => $client->user_id, 'package_id' => $package->id, 'module_id' => $module_id, 'status' => 3, 'coache_id' => $client->coache_id]);
                }
            }
        }
        return response()->json($package);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\package  $package
     * @return \Illuminate\Http\Response
     */
    public function destroy($package_id) {
        $package = package::find($package_id);
        $pack = package::destroy($package_id);
        return back()->with('pack', $pack)->with('success', $package->title . ' has been deleted successfully.');
    }

    public function showLinkedClients($package_id) {

        $coaches = \App\assignment::coach()->get()->unique('user_id');
	    $package = \App\package::find($package_id);
	    $client_exist = [];
	    $allClient = [];
	    foreach ($package->getClients() as $user)
	    {
		    $client_exist[] = $user['attributes']['id'];
	    }
	    foreach (\App\user::whereNotIn('id',$client_exist)->get() as $user)
	    {
		    if($user->isClient()){
			    $allClient[] = $user;
		    }
	    }

        return response()->json([
        	'all_clients' => $allClient,
             'clients' => $package->getClients(),
             'coach' => $package->getCoach(),
             'all_clients_coach'=>$coaches[0]->getCoacheClients(Auth::id())]);
    }

    /**
     * copy a package.
     *
     * @param  \App\package  $package
     * @return \Illuminate\Http\Response
     */
    public function makeCopy($package_id) {
        $package = package::find($package_id);
        $copy_of_package = $package->replicate();
        $copy_of_package->title = "[COPY OF] " . $copy_of_package->title;

        $copy_of_package->save();
//        
        $copy_of_package->modules()->saveMany($package->modules()->get());
        // auto set coache
        $assign = new \App\assign();
        $assign->coache(auth()->user()->id, $copy_of_package->id);

        return back();
    }

    public function updateStatus(Request $request, $package_id) {
       
        $response1 = $request->status_1;
        $response2 = $request->status_0;
        //dd('ahsan');
        $package = package::find($package_id);
        if($package->status == 1){
            $package->status = $response2;
        }
        else if($package->status == 0){
            $package->status = $response1;
        }

        $package->save();
        return back()->with('success', ' The status of "' . $package->title . ' " has been updated successfully.');
    }

    public function assignCoachForm(Request $request, $package_id) {
        $package = package::find($package_id);
        $coaches = \App\User::whereIn('status', [1, 2])->orderBy('name', 'asc')->get();
        $alreadyAssigned = \App\assignment::where('package_id', $package->id)
                        ->where("role_id", \App\role::coache())->pluck("user_id");
        $assignedCoaches = \App\User::whereIn("id", $alreadyAssigned)->get();
        return view('package.assign_coach')->with('package', $package)->with('coaches', $coaches)
                        ->with('assignedCoaches', $assignedCoaches);
    }

    public function assignCoach(Request $request) {
        $package = package::find($request->package_id);
  
     
        $assign = new \App\assign();

        $alreadyAssigned = \App\assignment::whereIn('user_id', $request->assignedCoaches)->where('package_id', $package->id)->where("role_id", \App\role::coache())->get();

        foreach ($request->assignedCoaches as $coachid) {
      
            if ($alreadyAssigned->where('user_id', $coachid)->count() < 1) {
           
                $assign->coache($coachid, $package->id, 4);

            
                $user = \App\User::find($coachid);
                \Mail::to($user->email)->send(new NewPackageAdded($user,$package));
            }
        }


        return redirect("packages/")->with('success', 'Coach(es) assigned successfully.');
    }

    public function view($package_id) {
        //
        $package = package::find($package_id);

//        
        $epackage = new package();
        $live_modules = module::where('is_live', true)->author()->get();

        return view('package.view')->with('epackage', $epackage)->with('state', 'update')
                        ->with('live_modules', $live_modules)->with("package", $package);
    }

    // Package Category Functions

    public function view_category() {
        if (\Auth::user()->isAdmin()){
            $package_category = package_category::get();
            return view('package.package_cat_view',['category' => $package_category]);
        }

   }

    public function add_category() {
        if (\Auth::user()->isAdmin()){
            return view('package.package_cat_add');
        }

    }

    public function store_category(Request $request) {
        if (\Auth::user()->isAdmin()){
            $title= $request->title;
            $package_category = package_category::create(['cat_name' => $title]);
            return  redirect()->route('view_package_category');
        }

    }
   
    public function edit_category($id) {
        if (\Auth::user()->isAdmin()){
            $package_category = package_category::find($id);
            return view('package.package_cat_add',['package_category'=> $package_category]);
        }

    }

    public function destroy_category($id) {
        if (\Auth::user()->isAdmin()){
            $package_category = package_category::find($id);
            $package_category->delete();
            return  redirect()->route('view_package_category');
        }

    }

    public function update_category(Request $request, $id) {
        if (\Auth::user()->isAdmin()){
            $title= $request->title;
            $package_category = package_category::find($id);
            $package_category->cat_name = $title ;
            $package_category->save();
            return redirect()->route('view_package_category');
        }

    }


  
      public function package_category(){
        if (\Auth::user()->isAdmin()){
            $count= [];
            $package_category = package_category::get();
            foreach($package_category as $category){
                $count[] = package_category::find($category->id)->package;
            }
            return view('package.package_category',['package_category'=>$package_category,'count'=>$count]); 
        }
      }


      public function get_packages_by_category($id){
        if (\Auth::user()->isAdmin()){
            $packages = package::owner()->active()->get();
            $live_modules = module::where('is_live', true)->author()->get();
            $package_category = package::where('package_category_id',$id)->get();
            $cat_id= $id;
            return view('package.index_new',['packages'=>$package_category,'live_modules'=> $live_modules,'cat_id'=>$cat_id]); 
        }
      }
      
      // Package Category Functions
}   
