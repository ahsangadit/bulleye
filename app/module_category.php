<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class module_category extends Model
{
    protected $table = 'module_category';
    protected $fillable = ['cat_name'];
    public $timestamps = false;

    public function module() {
        return $this->hasMany('App\module');
    }
}
