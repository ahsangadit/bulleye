-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 16, 2020 at 12:33 PM
-- Server version: 10.4.8-MariaDB
-- PHP Version: 7.3.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `bb_eye`
--

-- --------------------------------------------------------

--
-- Table structure for table `assignments`
--

CREATE TABLE `assignments` (
  `id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `package_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `module_id` int(10) UNSIGNED NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT 3 COMMENT '* status field can hold\n             * 1 for active module\n             * 2 for in progress module\n             * 3 for in pending module\n             * 4 for completion of module\n             ',
  `coache_id` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `assignments`
--

INSERT INTO `assignments` (`id`, `role_id`, `user_id`, `package_id`, `created_at`, `updated_at`, `module_id`, `status`, `coache_id`) VALUES
(2663, 3, 342, 96, '2020-01-15 19:16:44', '2020-01-15 19:16:44', 150, 3, NULL),
(2664, 2, 347, 96, '2020-01-15 19:23:01', '2020-01-15 19:23:01', 150, 3, 2663),
(2665, 2, 347, 96, '2020-01-15 19:23:01', '2020-01-15 19:23:01', 151, 3, 2663),
(2666, 2, 348, 96, '2020-01-15 22:03:24', '2020-01-15 22:03:24', 150, 3, 2663),
(2667, 2, 348, 96, '2020-01-15 22:03:24', '2020-01-15 22:03:24', 151, 3, 2663);

-- --------------------------------------------------------

--
-- Table structure for table `discussions`
--

CREATE TABLE `discussions` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `question_id` int(10) UNSIGNED NOT NULL,
  `response_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `assignment_id` int(10) UNSIGNED NOT NULL,
  `visibility` tinyint(4) NOT NULL DEFAULT 0 COMMENT '* status field can hold * 1 for client only * 0 for all	'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `discussions`
--

INSERT INTO `discussions` (`id`, `user_id`, `question_id`, `response_id`, `created_at`, `updated_at`, `assignment_id`, `visibility`) VALUES
(4696, 342, 1013, 4696, '2020-01-15 19:26:18', '2020-01-15 19:26:18', 2664, 1),
(4697, 342, 1013, 4697, '2020-01-15 19:26:42', '2020-01-15 19:26:42', 2664, 1),
(4698, 342, 1013, 4698, '2020-01-15 19:27:41', '2020-01-15 19:27:41', 2664, 0),
(4699, 342, 1013, 4699, '2020-01-15 22:04:52', '2020-01-15 22:04:52', 2664, 1),
(4700, 342, 1013, 4700, '2020-01-15 22:05:29', '2020-01-15 22:05:29', 2664, 0),
(4701, 342, 1013, 4701, '2020-01-15 22:06:23', '2020-01-15 22:06:23', 2666, 1),
(4702, 342, 1013, 4702, '2020-01-15 22:09:18', '2020-01-15 22:09:18', 2664, 1),
(4703, 342, 1013, 4703, '2020-01-15 22:09:29', '2020-01-15 22:09:29', 2664, 1);

-- --------------------------------------------------------

--
-- Table structure for table `documents`
--

CREATE TABLE `documents` (
  `id` int(10) UNSIGNED NOT NULL,
  `filename` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `uploaded_by` int(11) NOT NULL,
  `uploaded_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `module_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `documents`
--

INSERT INTO `documents` (`id`, `filename`, `description`, `uploaded_by`, `uploaded_at`, `created_at`, `updated_at`, `module_id`) VALUES
(450, 'BB_150.docx', 'Test', 342, '2020-01-16 07:46:15', '2020-01-15 18:46:15', '2020-01-15 18:46:15', 150);

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `modules`
--

CREATE TABLE `modules` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `is_live` tinyint(1) NOT NULL DEFAULT 0,
  `status` tinyint(1) NOT NULL DEFAULT 1,
  `author_id` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `modules`
--

INSERT INTO `modules` (`id`, `title`, `description`, `content`, `created_at`, `updated_at`, `is_live`, `status`, `author_id`) VALUES
(150, 'Test', 'Test', '<p>Test</p>', '2020-01-15 18:45:57', '2020-01-15 18:48:50', 1, 1, 342),
(151, 'Turtle Beach', 'Turtle Beach', '<p>Turtle Beach<br></p>', '2020-01-15 19:13:58', '2020-01-15 19:14:11', 1, 1, 342);

-- --------------------------------------------------------

--
-- Table structure for table `packages`
--

CREATE TABLE `packages` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `price` double(8,2) DEFAULT NULL,
  `currency` char(4) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `paymnent_frequency` enum('One Off','monthly','weekly','yearly') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `facebook_group` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `release_schedule` enum('deliver immediately','rolling launch','one off launch','on completion of previous') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT 0 COMMENT '* status field can hold* 1 for reply off * 0 for reply on'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `packages`
--

INSERT INTO `packages` (`id`, `title`, `description`, `price`, `currency`, `paymnent_frequency`, `facebook_group`, `release_schedule`, `created_at`, `updated_at`, `status`) VALUES
(96, 'Testing', '<p>Testing<br></p>', 23.00, 'AUD', 'One Off', 'Testing', NULL, '2020-01-15 19:16:44', '2020-01-15 22:08:59', 1);

-- --------------------------------------------------------

--
-- Table structure for table `package_module`
--

CREATE TABLE `package_module` (
  `module_id` int(10) UNSIGNED NOT NULL,
  `package_id` int(10) UNSIGNED NOT NULL,
  `id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `package_module`
--

INSERT INTO `package_module` (`module_id`, `package_id`, `id`, `created_at`, `updated_at`) VALUES
(150, 96, 741, '2020-01-15 19:16:44', '2020-01-15 19:16:44'),
(151, 96, 742, '2020-01-15 19:16:44', '2020-01-15 19:16:44');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `password_resets`
--

INSERT INTO `password_resets` (`email`, `token`, `created_at`) VALUES
('sadaf_cu@hotmail.com', '$2y$10$/DVMaSxxAXfII5oLcKyKy.ilydGF7PbAzT30LLG/Y6pZ23Iz6SoEu', '2017-05-22 16:51:34'),
('juliemcgloin@hotmail.com', '$2y$10$OSq9D.8zwqrbjLcY7fBnyuY/8yhwzPp8fyh2UaVrwABclLT4CBaVy', '2017-06-20 23:44:09'),
('kazim.raza@mksofttech.com', '$2y$10$bgFL/gGsIzoOOHiElpHTyedSXVBAESkQoOFTzE9gEG8Vwhdquie32', '2017-06-21 20:30:39'),
('zamin2212@gmail.com', '$2y$10$6RF/4akBqJ/E7pAqmV1kUeYCcAVeUI/hMB0DOsy199YB5nWyZUlcC', '2018-02-21 18:30:55'),
('info@regeneratehealthmc.com', '$2y$10$wDnqKmoSc8FJVuEXX2AbL..eO7T/r8Brmbgo3VI67t59OIavtGtJC', '2018-03-03 15:00:05'),
('jnelle.holland@gmail.com', '$2y$10$vtkv1dcvslufW4EdOLaVUOwYDDVau0pCd7cNT1Qnq8JBmRa7dyZnm', '2018-09-15 12:59:37'),
('paul.cashman.b87q@statefarm.com', '$2y$10$UXtdKVZOgrPP0aG86P1WNujl1GxtvCr4M1qom7xP30shDztP8wy.C', '2019-02-05 13:14:35'),
('muhammadarsalan11092@gmail.com', '$2y$10$rSacATcB2HtnwLm7CUFQtemVrtqHs4n6EAQP/AASTbFJRZFAj99SO', '2019-02-07 07:22:10'),
('kr_sayani@outlook.com', '$2y$10$ucIB4L9TTzzojKuE.sImf.Gjw.xf0N4DrK9L7NuPXxRj.MPHi17xe', '2019-02-07 07:43:23'),
('kr_sayani@hotmail.com', '$2y$10$DdKCQhoBFaqIvtTYU6G3D.HR0meTFHTWbHhoeZFpireYPhOCH.g1q', '2019-02-07 07:50:11'),
('faizanshaikh@hztech.biz', '$2y$10$YwwhItzHGMdTC4EjvMIV3.4U2/iE4WaIfSMi0/LRl8nA4zkhpNHWS', '2019-02-07 09:37:13'),
('nilton.rodrigues.fgbo@statefarm.com', '$2y$10$rzSiReCUbERtPRwFlYs1j.BDfIx0g6zQ/YUaSBqEM.bqYxPNYWrH.', '2019-05-16 16:46:57'),
('shahin.chear.rnpk@statefarm.com', '$2y$10$/AqeOjrPIAVwoHpbVSeDHO3vKxOByn0jrh35fV8SrDJlTu7WX2bp6', '2019-05-20 04:26:42'),
('paul.lavelle.j8ov@statefarm.com', '$2y$10$V3tZG3.wFS14wGoP2ufbyeqnJOj7RCGxhPOuab7/TXyZIx3QQvYMa', '2019-06-05 13:28:48'),
('linda.horton.nsd4@statefarm.com', '$2y$10$pDyKF6cPwmSBfZ//K38ZpemR0w6Zlqx5xr0xT0NU9nDdwamYOFS32', '2019-07-01 01:43:15'),
('wendy.m.baker@fbfs.com', '$2y$10$XeoNfTHdPH7Zvglv09hQmu16tpCZ1D/eBiCdnfe0wpmaKp2IBunK.', '2019-07-11 14:54:42'),
('kelly.lux.gjcg@statefarm.com', '$2y$10$qXbu6BnNrSPuQhP3xqb36OAqGYSBdzPT6FQ/vIqSCRNrDLR8IllSm', '2020-01-08 17:32:17');

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE `permissions` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `permission_role`
--

CREATE TABLE `permission_role` (
  `id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL,
  `permission_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `questions`
--

CREATE TABLE `questions` (
  `id` int(10) UNSIGNED NOT NULL,
  `sno` int(11) NOT NULL,
  `content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `module_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `questions`
--

INSERT INTO `questions` (`id`, `sno`, `content`, `created_at`, `updated_at`, `module_id`) VALUES
(1013, 1, '<p>Test</p>', '2020-01-15 18:46:07', '2020-01-15 18:46:07', 150);

-- --------------------------------------------------------

--
-- Table structure for table `responses`
--

CREATE TABLE `responses` (
  `id` int(10) UNSIGNED NOT NULL,
  `content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `responses`
--

INSERT INTO `responses` (`id`, `content`, `created_at`, `updated_at`) VALUES
(4695, 'hello world', '2020-01-15 19:10:31', '2020-01-15 19:10:31'),
(4696, 'hie', '2020-01-15 19:26:18', '2020-01-15 19:26:18'),
(4697, 'hrtrr', '2020-01-15 19:26:42', '2020-01-15 19:26:42'),
(4698, 'hie', '2020-01-15 19:27:41', '2020-01-15 19:27:41'),
(4699, 'hello', '2020-01-15 22:04:52', '2020-01-15 22:04:52'),
(4700, 'hello', '2020-01-15 22:05:29', '2020-01-15 22:05:29'),
(4701, 'hello', '2020-01-15 22:06:23', '2020-01-15 22:06:23'),
(4702, 'hello', '2020-01-15 22:09:18', '2020-01-15 22:09:18'),
(4703, 'jjknjk', '2020-01-15 22:09:29', '2020-01-15 22:09:29');

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `desc` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `desc`, `created_at`, `updated_at`) VALUES
(1, 'admin', '', NULL, NULL),
(2, 'client', '', NULL, NULL),
(3, 'coache', '', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `avatar` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'default.jpg',
  `description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`, `avatar`, `description`, `status`) VALUES
(342, 'ahsan amin', 'ahsan.amin334@gmail.com', '$2y$10$Vb07Pi09RUZDcdjSBMD2iuV31l2QoRmP20GHYqMohd3Gt7Ssxx9iG', NULL, '2020-01-15 18:45:29', '2020-01-15 18:45:29', 'default.jpg', NULL, 1),
(347, 'yasin', 'yasin@gmail.com', '$2y$10$4MI3nXJg1Ze2HbzqmajR.eja7Bq65Jiwr5Uq4wEqXBwZu7iBe.sIu', NULL, '2020-01-15 19:22:55', '2020-01-15 19:22:55', 'default.jpg', NULL, 0),
(348, 'Tahseen', 'tahseen@gmail.com', '$2y$10$U4t48SA2Y9EpiCDFO2sh8.AKJHomE/qZl9TS7KFrfHhAIckmR8Ppa', NULL, '2020-01-15 22:03:18', '2020-01-15 22:03:18', 'default.jpg', NULL, 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `assignments`
--
ALTER TABLE `assignments`
  ADD PRIMARY KEY (`id`),
  ADD KEY `role_user_package_role_id_index` (`role_id`),
  ADD KEY `role_user_package_user_id_index` (`user_id`),
  ADD KEY `role_user_package_package_id_index` (`package_id`),
  ADD KEY `assignments_module_id_index` (`module_id`),
  ADD KEY `assignments_coache_id_index` (`coache_id`);

--
-- Indexes for table `discussions`
--
ALTER TABLE `discussions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_response_question_user_id_index` (`user_id`),
  ADD KEY `user_response_question_question_id_index` (`question_id`),
  ADD KEY `user_response_question_response_id_index` (`response_id`),
  ADD KEY `discussions_assignment_id_index` (`assignment_id`);

--
-- Indexes for table `documents`
--
ALTER TABLE `documents`
  ADD PRIMARY KEY (`id`),
  ADD KEY `documents_module_id_index` (`module_id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `modules`
--
ALTER TABLE `modules`
  ADD PRIMARY KEY (`id`),
  ADD KEY `modules_author_id_foreign` (`author_id`);

--
-- Indexes for table `packages`
--
ALTER TABLE `packages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `package_module`
--
ALTER TABLE `package_module`
  ADD KEY `package_module_module_id_index` (`module_id`),
  ADD KEY `package_module_package_id_index` (`package_id`),
  ADD KEY `id` (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`),
  ADD KEY `password_resets_token_index` (`token`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `permission_role`
--
ALTER TABLE `permission_role`
  ADD PRIMARY KEY (`id`),
  ADD KEY `permission_role_role_id_index` (`role_id`),
  ADD KEY `permission_role_permission_id_index` (`permission_id`);

--
-- Indexes for table `questions`
--
ALTER TABLE `questions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `questions_module_id_index` (`module_id`);

--
-- Indexes for table `responses`
--
ALTER TABLE `responses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `assignments`
--
ALTER TABLE `assignments`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2668;

--
-- AUTO_INCREMENT for table `discussions`
--
ALTER TABLE `discussions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4704;

--
-- AUTO_INCREMENT for table `documents`
--
ALTER TABLE `documents`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=451;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `modules`
--
ALTER TABLE `modules`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=152;

--
-- AUTO_INCREMENT for table `packages`
--
ALTER TABLE `packages`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=97;

--
-- AUTO_INCREMENT for table `package_module`
--
ALTER TABLE `package_module`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=743;

--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `permission_role`
--
ALTER TABLE `permission_role`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `questions`
--
ALTER TABLE `questions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1014;

--
-- AUTO_INCREMENT for table `responses`
--
ALTER TABLE `responses`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4704;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=349;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `assignments`
--
ALTER TABLE `assignments`
  ADD CONSTRAINT `assignments_coache_id_foreign` FOREIGN KEY (`coache_id`) REFERENCES `assignments` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `assignments_module_id_foreign` FOREIGN KEY (`module_id`) REFERENCES `modules` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `role_user_package_package_id_foreign` FOREIGN KEY (`package_id`) REFERENCES `packages` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `role_user_package_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `role_user_package_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `discussions`
--
ALTER TABLE `discussions`
  ADD CONSTRAINT `discussions_assignment_id_foreign` FOREIGN KEY (`assignment_id`) REFERENCES `assignments` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `user_response_question_question_id_foreign` FOREIGN KEY (`question_id`) REFERENCES `questions` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `user_response_question_response_id_foreign` FOREIGN KEY (`response_id`) REFERENCES `responses` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `user_response_question_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `documents`
--
ALTER TABLE `documents`
  ADD CONSTRAINT `documents_module_id_foreign` FOREIGN KEY (`module_id`) REFERENCES `modules` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `modules`
--
ALTER TABLE `modules`
  ADD CONSTRAINT `modules_author_id_foreign` FOREIGN KEY (`author_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `package_module`
--
ALTER TABLE `package_module`
  ADD CONSTRAINT `package_module_module_id_foreign` FOREIGN KEY (`module_id`) REFERENCES `modules` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `package_module_package_id_foreign` FOREIGN KEY (`package_id`) REFERENCES `packages` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `permission_role`
--
ALTER TABLE `permission_role`
  ADD CONSTRAINT `permission_role_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `permission_role_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `questions`
--
ALTER TABLE `questions`
  ADD CONSTRAINT `questions_module_id_foreign` FOREIGN KEY (`module_id`) REFERENCES `modules` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
